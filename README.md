# DSC Dongle Utility 1.8

## Windows

1. Download DSC Utility from [here](https://gitlab.leegality.com/leegality-public/dsc-utility/raw/master/leegality-dsc-client-setup.exe?inline=false).
2. Double click on downloaded file *leegality-dsc-client-setup.exe* to install.
3. On successful installation DSC Utility will start automatically.
4. Utility will run automatically on every time on system startup.

## Mac

1. Download DSC Utility from [here](https://gitlab.leegality.com/leegality-public/dsc-utility/raw/master/LeegalityDSCClient.pkg?inline=false).
2. Double click on downloaded file *LeegalityDSCClient.pkg* to install.
3. On successful installation DSC Utility will start automatically.
4. Utility will run automatically on every time on system startup.
